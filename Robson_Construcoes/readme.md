Cadastros de listas e geração de relatórios.

Adiciona lista de cargos e funcionários, mostra relatórios com todas as informações de funcionários e calcula o valor total dos salários de cada cargo.

Feito com C# 8.0 (.NET Framework 4.7.2) no VS2019 versão 16.7.6.