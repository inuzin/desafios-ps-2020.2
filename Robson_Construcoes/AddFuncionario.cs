﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robson_Construcoes
{
    class AddFuncionario
    {
        AddCargo addcargo = new AddCargo();
        private List<Funcionario> funcionarios = new List<Funcionario>();

        public List<Funcionario> GetFuncionarios()
        {
        return funcionarios;
        }     

        public void NovoFuncionario(AddCargo addcargo, int codigo, string nome, int codigo_cargo)
        {
            
            //Confere se o codigo ja esta sendo usado por algum funcionario na lista
            if (funcionarios.Count != 0)
            {
                for (int i = 0; i <= funcionarios.Count() - 1; i++)
                {
                    if (funcionarios[i].GetCodigo() == codigo)
                    {
                        return;
                    }
                }
            }
            //Confere se o cargo existe
            if (addcargo.GetCargos().Count() - 1 < codigo_cargo)
            {
                return;
            }

            funcionarios.Add(new Funcionario(codigo, nome, codigo_cargo));
        }
        
    }       
}
