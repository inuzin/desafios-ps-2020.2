﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Encontre_o_maior_numero
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 5, y = 10;
            Boolean maior_valor = (x > y);
            Boolean testTrue = maior_valor && PrintTrue(x,y);
            _ = testTrue || PrintFalse(x, y);
            
            Console.ReadLine();
        }
        static Boolean PrintTrue(int x, int y)
        {
            Console.WriteLine("O numero " + x + " e maior que o numero " + y);
            return true;
        }

        static Boolean PrintFalse(int x, int y)
        {
            Console.WriteLine("O numero " + y + " e maior que o numero " + x);
            return true;
        }
    }
}
