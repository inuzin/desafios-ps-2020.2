Encontrar o indice antecedente da maior sequencia de 1 em uma array de numeros binarios.

Pegando a array original, troca-se cada indice 0 por 1 e itera sobre cada uma dessas modificacoes contando as sequencias de 1 e o indice da maior sequencia. Ao final de cada iteracao a array retorna ao estado original.

Feito com C# 8.0 (.NET Framework 4.7.2) no VS2019 versão 16.7.6.