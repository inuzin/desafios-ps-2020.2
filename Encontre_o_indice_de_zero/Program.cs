﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encontre_o_indice_de_zero
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = { 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1 };
            int inc = 0;
            int seq = 0;
            int indice = 0;

            //Itera sobre a array inicial e detecta os zeros, mudando-os para 1
            for(int i = 0; i <= a.Count() -1; i++)
            {
                if(a[i] == 0)
                {
                    a[i] = 1;

                    //Itera sobre a array modificada
                    for(int j = 0; j <= a.Count() -1; j++)
                    {
                        //Detecta os indices com 1 e soma as sequencias
                        if(a[j] == 1)
                        {
                            inc += 1;
                        }
                        else
                        {
                            inc = 0;
                        }

                        //Caso exista uma sequencia maior, registrar a sequencia e o indice do 1 que a inicia
                        if(inc > seq)
                        {
                            seq = inc;
                            indice = i;
                        }

                    }

                    //Retorna a array ao estado original
                    a[i] = 0;
                }
            }

            Console.WriteLine("Indice: " + (indice + 1) + " Maior sequencia de 1: " + seq);
            Console.ReadLine();
        }
    }
}
