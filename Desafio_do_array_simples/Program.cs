﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desafio_do_array_simples
{
    class Program
    {
        static void Main(string[] args)
        {
            GerenciadorCotacao gerenciadorcotacao = new GerenciadorCotacao();

            gerenciadorcotacao.AddCotacao(30203);
            gerenciadorcotacao.AddCotacao(32163);
            gerenciadorcotacao.AddCotacao(95828);
            gerenciadorcotacao.AddCotacao(72020);

            gerenciadorcotacao.RemoveCotacao(2);

            gerenciadorcotacao.AlterarCotacao(0, 69392);

            for(int i = 0; i <= gerenciadorcotacao.GetCotacoes().Count() - 1; i++)
            {
                Console.WriteLine("Titulo: " + gerenciadorcotacao.GetCotacoes()[i].GetTitulo());
            }

            Console.ReadLine();
        }
    }
}
