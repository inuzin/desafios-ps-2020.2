﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desafio_do_array_simples
{
    class GerenciadorCotacao
    {
        private List<Cotacao> cotacoes = new List<Cotacao>();

        public List<Cotacao> GetCotacoes()
        {
         return cotacoes;
        }

        public void AddCotacao(double Titulo)
        {
         cotacoes.Add(new Cotacao(Titulo));
        }

        public void RemoveCotacao(int index)
        {
         cotacoes.RemoveAt(index);
        }

        public void AlterarCotacao(int index, double Titulo)
        {
         cotacoes[index].SetTitulo(Titulo);
        }
    }
}
